# Supreme Mock Server Test App

A simple React+Vite+Typescript app to test the `supreme-mock-server` package.

## Installation

Due to one of the package _technically_ not being compatible, a forced install is necessary:

```bash
npm i --force
```

> The package in question is the `react-json-view` package
