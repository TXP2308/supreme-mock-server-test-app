import "./App.css";
import { Button } from "@/components/ui/button";
import { Card } from "@/components/ui/card";
import { initializeMockServer } from "@pizzahut/supreme-mock-server";
import { useState } from "react";
import ReactJson from "react-json-view";

const endpoints = [
  "Get /phdapi/v1/customer",
  "Post /phdapi/v1/yum-guest",
  "Post /ordering-gateway/carts",
  "Get /ordering-gateway/carts",
  "Post /phdapi/v2/stores/search",
  "Post /ordering-gateway/carts/items",
  "Get /phdapi/v2/stores/311016",
  "Get /fake-endpoint",
];

const menuCategories = [
  "products",
  "bundleChoices",
  "bundles",
  "categories",
  "modifiers",
  "modifierWeights",
  "optionTypes",
  "optionValues",
  "slots",
  "upsells",
];

function App() {
  const [data, setData] = useState<any[]>([]);
  const [selectedEndpoint, setSelectedEndpoint] = useState<string>("");
  initializeMockServer({
    mockEndpoints: [
      {
        method: "get",
        url: "/phdapi/v1/customer",
        data: { message: "This is an override endpoint", customer_id: "123" },
      },
      {
        method: "get",
        url: "/fake-endpoint",
        data: { message: "This is a fake endpoint" },
      },
    ],
  });

  const handleClick = (endpoint: string) => {
    const endpointArray = endpoint.split(" ");
    fetch(`https://services-staging.digital.pizzahut.com${endpointArray[1]}`, {
      method: endpointArray[0],
    })
      .then((res) => res.json())
      .then((data) => {
        setSelectedEndpoint(endpoint);
        setData(data);
      });
  };

  const handleMenuClick = (category: string) => {
    fetch(
      `https://services-staging.digital.pizzahut.com/content-gateway/stores/311016/menu`
    )
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        setSelectedEndpoint(`/content-gateway/stores/*/menu -> ${category}`);
        setData(data[category]);
      });
  };

  return (
    <>
      <h1 className="text-lg font-bold mb-4">Endpoints</h1>
      <div className="w-full flex flex-wrap">
        {endpoints.map((endpoint) => (
          <Button
            key={endpoint}
            className="mr-2 mb-1 text-xs"
            onClick={() => handleClick(endpoint)}
          >
            {endpoint}
          </Button>
        ))}
      </div>
      <h1 className="text-lg font-bold">Menu Sections</h1>
      <p className="text-xs text-gray-400 mb-4">
        Displaying the entire menu at once will cause the app to crash. They can
        sometimes take up to a minute to load.
      </p>
      {menuCategories.map((category) => (
        <Button
          key={category}
          className="mr-2 mb-1 text-xs"
          onClick={() => handleMenuClick(category)}
        >
          {category}
        </Button>
      ))}
      {selectedEndpoint && (
        <Card className="mt-4 p-4 ">
          <h1 className="text-lg font-bold text-center mb-4">
            {selectedEndpoint}
          </h1>
          <ReactJson src={data} />
        </Card>
      )}
    </>
  );
}

export default App;
